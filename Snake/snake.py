import pygame
from random import randint
import math

pygame.init()

clock = pygame.time.Clock()

white = (255,255,255)
black = (0,0,0)
red = (255, 0, 0)
green = (0, 255, 0)
blue = (0, 0, 255)
dodgerblue = (30, 144, 255)
gray = (128,128,128)

#font
myfont = pygame.font.SysFont("monospace",15)

windowX = 800
windowY = 600
gameDisplay = pygame.display.set_mode((windowX,windowY))
pygame.display.set_caption('Snake')
#pygame.display.flip() updates entire surface at once vs. update which can update a specific part via param

gameExit = False
restart = False

lead_x = 400
lead_y = 300
lead_x_direct = 0 #go north initially
lead_y_direct = 0
collected = True #If the reward ramains untouched
tail_pieces = [] #list of tail object squares with coordinates
length = 0
begin = 0
gameStart = 0
restr = 0
mousetimer = 0
mouseEnRoute = False #See if a mouse is moving on a subpath
score = 0

#text definitions
gameOver_l = myfont.render("GAME OVER", 1, black)
gameOver_opt = myfont.render("Space to restart               Esc to go to main menu", 1, black)
paused_l = myfont.render("PAUSED", 1, black)
controls_l = myfont.render("[SPACE] - Pause       [ESC] - End Game        Arrow keys to move", 1, black)

#initial window setup

class location: #Used for rewards and tail nodes
    def __init__(self, x, y):
        self.x = x
        self.y = y

"""
Function: pickRandom
Purpose: Runs whenever a reward needs to find a location to be placed
Makes sure the reward is not placed on an existing tail fragment
Params: tail_pieces(list)
"""
def pickRandom(tail_pieces):
    success = False
    while success == False: #Run while loop until proper place for reward is picked
        picked_on_tail = 0 #The random generated a reward onto a tail, pick new
        x = (randint(20,windowX-30))
        y = (randint(20,windowY-30))
        x_f = math.floor(x/10)*10
        y_f = math.floor(y/10)*10
        coords = location(x_f,y_f)
        for x in range(0, len(tail_pieces)):
            if x_f == tail_pieces[x].x and y_f == tail_pieces[x].y:
                picked_on_tail = 1
        if picked_on_tail == 0:
            success = True
                
    return coords

"""
Function: gameOver
Purpose: Runs whenever the player presses ESC to end the game or runs into a wall
displays the score at the end as well
Params: Score(int)
"""
def gameOver():
    gameDisplay.blit(gameOver_l, (350, 300))
    gameDisplay.blit(gameOver_opt, (175, 325))
    pygame.display.update()
    exitG = False
    option = False

    #Reset values

    #empty list of tail pieces
    for x in range(0, len(tail_pieces)):
        temp = tail_pieces.pop()

    #empty player vars
    global length 
    length = 0
    global lead_x
    lead_x = 400
    global lead_y
    lead_y = 300
    global collected
    collected = True
    global begin
    begin = 0
    global lead_y_direct
    lead_y_direct = 0
    global lead_x_direct
    lead_x_direct = 0
    global score
    score = 0
    global mousetimer
    mousetimer = 0
    global gameStart
    gameStart = 0
    
    while not option:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                exitG = True
                option = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    option = True
                    
                if event.key == pygame.K_ESCAPE:
                    return (1)
                    
    if exitG == True:
        pygame.quit()
        quit()

"""
Function: pause
Purpose: Ran when the game is paused, check for unpause or quit
minor annoyance: in order to not have "game over" overlap "pause"
must pass in draw params in order to clear
Params: lead_x, lead_y, length, reward, tail_pieces, score
"""
def pause(reward):
    paused = True
    exitG = False
    scorehead_l = myfont.render("SCORE: ", 1, black)
    gameDisplay.blit(scorehead_l, (345, 335))
    gameDisplay.blit(paused_l, (350, 300))
    score_paused_l = myfont.render(str(score), 1, black)
    gameDisplay.blit(score_paused_l, (405, 335))
    pygame.display.update()
    while paused:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    paused = False
                if event.key == pygame.K_ESCAPE:
                    exitG = True
                    paused = False
    

    if exitG == True:
        drawGame(reward,True)
        choice = gameOver()
        return choice

    return 0

"""        
Function: drawGame
Purpose: Update for frame, new placement of playersnake and reward
Params: reward(int) - score, end(bool) - boolean for last frame of game, doesn't move trail piece if true
(a minor annoyance basically)
"""
def drawGame(reward,end):
    global mousetimer
    gameDisplay.fill(dodgerblue)
    scorehead_l = myfont.render("SCORE: ", 1, black)
    score_l = myfont.render(str(score), 1, black)
    gameDisplay.blit(scorehead_l, (30, 4))
    gameDisplay.blit(score_l, (85, 4))
    gameDisplay.blit(controls_l, (85, 580))

    pygame.draw.rect(gameDisplay, white, [20,20,760,560])
    pygame.draw.rect(gameDisplay, green, [lead_x,lead_y,10,10])
    if length > 0 and not end:
        #Now move the last node in the array to the blank space where the tail was
        tail_pieces[length-1].x = lead_x - lead_x_direct
        tail_pieces[length-1].y = lead_y - lead_y_direct
        #it's also now the first in the list
        neck = tail_pieces.pop()
        tail_pieces.insert(0, neck)            
    if mousetimer > 0 or mousetimer == -1:
        pygame.draw.rect(gameDisplay, gray, [reward.x, reward.y,10,10])
        mousetimer_l = myfont.render(str(mousetimer), 1, black)
        if mousetimer > 0: #don't draw a -1 above the mouse, happens quick but can be seen rarely
            gameDisplay.blit(mousetimer_l, (reward.x, reward.y-15))
        if mousetimer == -1:
            mousetimer = 0
    else:
        pygame.draw.rect(gameDisplay, red, [reward.x, reward.y, 10,10])
    for x in range(0, len(tail_pieces)):
        pygame.draw.rect(gameDisplay, black, [tail_pieces[x].x, tail_pieces[x].y,10,10])
    pygame.display.update()



"""
Function: incrementalLine
Purpose: works with animateSnake (only, at the moment) in order to animate a line given destination
and direction
Params: start(int), end(int) - destination, size - the size of the "pixel" of animation  
vh_bool - boolean for orientation, increment - what iteration of the frame the animation is at
"""
def incrementalLine(start,end,size,length,vh_bool,start_x,start_y):
    #Define:
    #vh_bool = 0 = horiz
    #vh_bool = 1 = vert
    leftright = 0 #movement constants, one is always zero
    updown = 0
    if vh_bool == 0:
        if start > end:
            leftright = -size
            updown = 0
        else:
            leftright = size
            updown = 0
    else:
        if start > end:
            leftright = 0
            updown = -size
        else:
            leftright = 0
            updown = size

    for i in range(0,length):
        pygame.draw.rect(gameDisplay, green, [start_x+(leftright*i),start_y+(updown*i),size,size])
        

"""
Function: animateSnake
Purpose: Draws the animated snake title for the main menu
"""
def animateSnake(frame):
    #easy change for animation
    start_x = 250
    start_y = 200
    size = 10
    length = 0
    if frame >= 0:
        if frame < 40:
            length = 0
            length = int(frame/10) + 1
        else:
            length = 4
        incrementalLine(250,210,size,length,0,start_x,start_y)

    #The letter S
    if frame > 40:
        if frame < 60:
            length = 0
            length = int((frame-40)/10) + 1
        else:
            length = 2
        incrementalLine(200,240,size,length,1,start_x-30,start_y+10)

    if frame > 60:
        if frame < 90:
            length = 0
            length = int((frame-60)/10) + 1
        else:
            length = 3
        incrementalLine(220,240,size,length,0,start_x-20,start_y+20)

    if frame > 90:
        if frame < 110:
            length = 0
            length = int((frame-90)/10) + 1
        else:
            length = 2
        incrementalLine(230,240,size,length,1,start_x,start_y+30)

    if frame > 110:
        if frame < 140:
            length = 0
            length = int((frame-110)/10) + 1
        else:
            length = 3
        incrementalLine(240,210,size,length,0,start_x-10,start_y+40)

    #The letter N
    start_x = 280
    start_y = 240
    if frame > 140:
        if frame < 190:
            length = 0
            length = int((frame-140)/10) + 1
        else:
            length = 5
        incrementalLine(240,200,size,length,1,start_x,start_y)

    if frame > 190:
        if frame < 210:
            length = 0
            length = int((frame-190)/10) + 1
        else:
            length = 2
        incrementalLine(200,210,size,length,1,start_x+10,start_y-40)

    if frame > 210:
        if frame < 230:
            length = 0
            length = int((frame-210)/10) + 1
        else:
            length = 2
        incrementalLine(210,220,size,length,1,start_x+20,start_y-30)

    if frame > 230:
        if frame < 250:
            length = 0
            length = int((frame-230)/10) + 1
        else:
            length = 2
        incrementalLine(220,230,size,length,1,start_x+30,start_y-20)

    if frame > 250:
        if frame < 270:
            length = 0
            length = int((frame-250)/10) + 1
        else:
            length = 2
        incrementalLine(230,240,size,length,1,start_x+40,start_y-10)

    if frame > 270:
        if frame < 320:
            length = 0
            length = int((frame-270)/10) + 1
        else:
            length = 5
        incrementalLine(240,200,size,length,1,start_x+50,start_y)
        
    
"""
Function: main
Purpose: Draws the main menu for the game with options for regular and extreme mode
"""
def main():
    choice = False
    rollover = False #Holding down left click and rolling over button wont activate
    option = -1
    snake_title_frame = 0
    while not choice:

        button1_col = white
        button2_col = white
        hiscores_col = white

        p = pygame.mouse.get_pos()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                exit()
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    pygame.quit()
                    exit()


        if not pygame.mouse.get_pressed()[0]:
            rollover = False
            
        if pygame.mouse.get_pressed()[0] and (p[0] < 155 or ( p[0] > 350 and p[0] < 455) or p[0] > 650) and (p[1] < 300 or p[1] > 400):
            rollover = True
            
        if p[0] > 155 and p[0] < 350 and p[1] > 300 and p[1] < 400:
            button1_col = red
            if pygame.mouse.get_pressed()[0] and not rollover:
                option = 1
                return option
                
        elif p[0] > 455 and p[0] < 650 and p[1] > 300 and p[1] < 400:
            button2_col = red
            if pygame.mouse.get_pressed()[0] and not rollover:
                print("AYY")

        elif p[0] > 305 and p[0] < 500 and p[1] > 450 and p[1] < 525:
            hiscores_col = blue
            if pygame.mouse.get_pressed()[0] and not rollover:
                print("AYY")
                
        gameDisplay.fill(dodgerblue)
        pygame.draw.rect(gameDisplay, white, [20,20,760,560])

        pygame.draw.rect(gameDisplay, black, [155,300,195,100])
        pygame.draw.rect(gameDisplay, button1_col, [165,310,175,80])

        pygame.draw.rect(gameDisplay, black, [455,300,195,100])
        pygame.draw.rect(gameDisplay, button2_col, [465,310,175,80])

        pygame.draw.rect(gameDisplay, black, [305,450,195,75])
        pygame.draw.rect(gameDisplay, hiscores_col, [315,460,175,55])
        
        gameOpt_l = myfont.render("Regular Mode                      Extreme Mode",1,black)
        hiscores_l = myfont.render("Hiscores",1,black)
        gameDisplay.blit(gameOpt_l, (195,335))
        gameDisplay.blit(hiscores_l, (360,485))
        animateSnake(snake_title_frame)
        pygame.display.update()
        clock.tick(60)
        
        snake_title_frame += 1



if __name__ == "__main__":

    while(1):
        option = main()
        if option == 1:
            while not restart:
                choice = 0
                for event in pygame.event.get():
                    if event.type == pygame.QUIT:
                        exit()
                    if event.type == pygame.KEYDOWN:
                        if event.key == pygame.K_UP:
                            lead_y_direct = -10
                            lead_x_direct = 0
                            gameStart = 1
                        if event.key == pygame.K_DOWN:
                            lead_y_direct = 10
                            lead_x_direct = 0
                            gameStart = 1
                        if event.key == pygame.K_LEFT:
                            lead_x_direct = -10
                            lead_y_direct = 0
                            gameStart = 1
                        if event.key == pygame.K_RIGHT:
                            lead_x_direct = 10
                            lead_y_direct = 0
                            gameStart = 1
                        if event.key == pygame.K_ESCAPE:
                            choice = gameOver()
                        if event.key == pygame.K_SPACE and gameStart == 1:
                            choice = pause(reward)
                            

                if choice == 1:
                    break

                #This runs when a reward has been picked up, replacing it with a new one and adding a tail pieces on
                if collected == True:
                    reward = pickRandom(tail_pieces)
                    #is the reward a mouse?
                    if length > 1 and ((length) % 5 == 0): #every 10th reward is a mouse
                        print(length)
                        mousetimer = 100
                    collected = False
                    
                    #If the player has moved, starting the game essentially
                    if begin == 1:
                        length += 1
                        if length == 1: 
                            tail_pieces.append(location(lead_x,lead_y))
                        else:
                            tail_pieces.append(location(tail_pieces[len(tail_pieces)-1].x, tail_pieces[len(tail_pieces)-1].y))

                    #The game has started
                    begin = 1

                lead_x += lead_x_direct
                lead_y += lead_y_direct

                #When a mouse is in play
                if mousetimer > 0:
                    #On first turn (maxmousetimer) do not move mouse but do after
                    if mousetimer < 100:
                        pickedMouseDirect = False #Make sure mouse doesn't go OOB
                        while not pickedMouseDirect:
                            #Pick random direction, 0-3 for NSWE, 0-7 for 8 square movement (harder)
                            mousedirect = (randint(0,3))
                            if mousedirect == 0 and reward.y > 30: #up
                                reward.y -= 10
                                break
                            elif mousedirect == 1 and reward.y < 570: #down
                                reward.y += 10
                                break
                            elif mousedirect == 2 and reward.x > 30: #left
                                reward.x -= 10
                                break
                            elif mousedirect == 3 and reward.x < 770: #right
                                reward.x += 10
                                break
                        
                    mousetimer -= 1


                for x in range(0, len(tail_pieces)):
                    if tail_pieces[x].x == reward.x and tail_pieces[x].y == reward.y:
                        collected = True
                        if mousetimer > 0:
                            score += 5
                            mousetimer = -1

                choice = 0
                #collision
                if lead_x < 20 or lead_x > 770 or lead_y < 20 or lead_y > 570:
                    choice = gameOver()
                    gover = 1
                    #empty all variables

                if choice == 1:
                    break

                #The player got the reward
                if lead_x == reward.x and lead_y == reward.y:
                    collected = True
                    #Reset the mousetimer if mouse was in play
                    if mousetimer > 0:
                        score += 5
                        mousetimer = -1
                    else:
                        score += 1

                #check collision of tail too
                gover = 0
                for x in range(0, len(tail_pieces)):
                    if lead_x == tail_pieces[x].x and lead_y == tail_pieces[x].y:
                        gameOver()
                        #empty all variables
                    

                        
                        gover = 1
                        break

                if gover == 1: #so we don't draw the game without needing to
                    continue
                        
                drawGame(reward,False)
                clock.tick(15)
